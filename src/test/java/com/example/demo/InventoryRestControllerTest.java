package com.example.demo;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.TRStatus;
import com.example.demo.sales.domain.TentativeReservation;
import com.example.demo.sales.domain.TentativeReservationRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class) // Check if the name of this class is correct or not
@WebAppConfiguration
@DirtiesContext
public class InventoryRestControllerTest {
    @Autowired
    PlantInventoryEntryRepository repo;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    TentativeReservationRepository tentativeReservationRepository;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired @Qualifier("_halObjectMapper")
    ObjectMapper mapper;

    @Test
    @Sql("/plants-dataset.sql")
    public void testTooLateSubmission () throws Exception {
        TentativeReservation reservation = new TentativeReservation();
        reservation.setRequestDate(BusinessPeriod.of(LocalDate.now(), LocalDate.now().plusDays(1)));


        tentativeReservationRepository.save(reservation);
        PlantReservation reservation1 = new PlantReservation();

        reservation1.setSchedule(BusinessPeriod.of(LocalDate.now().plusDays(4), LocalDate.now().plusDays(6)));

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                1L,
                LocalDate.now().plusDays(1),
                LocalDate.now().plusDays(3));

        if(availableItems.size() != 0) {
            reservation1.setPlant(availableItems.get(0));
        }


        plantReservationRepository.save(reservation1);

        MvcResult result = mockMvc.perform(post("/items/1/requestTentativeReservation")
                .content(mapper.writeValueAsString(reservation))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();



        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
//

    }

    @Test
    @Sql("/plants-dataset.sql")
    public void testInterval() throws Exception {
        TentativeReservation reservation = new TentativeReservation();
        reservation.setRequestDate(BusinessPeriod.of(LocalDate.now().plusDays(10), LocalDate.now().plusDays(18)));
        tentativeReservationRepository.save(reservation);

        PlantReservation reservation1 = new PlantReservation();

        reservation1.setSchedule(BusinessPeriod.of(LocalDate.now().plusDays(10), LocalDate.now().plusDays(19)));

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                1L,
                LocalDate.now().plusDays(1),
                LocalDate.now().plusDays(3));

        if(availableItems.size() != 0) {
            for(PlantInventoryItem item: availableItems){
                if (item.getId() == 1L){
                    reservation1.setPlant(item);
                }
            }
        }
        plantReservationRepository.save(reservation1);

        MvcResult result = mockMvc.perform(post("/items/1/requestTentativeReservation")
                .content(mapper.writeValueAsString(reservation))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();



        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
//

    }


    @Test
    @Sql("/plants-dataset.sql")
    public void successRequest() throws Exception {
        TentativeReservation reservation = new TentativeReservation();
        reservation.setRequestDate(BusinessPeriod.of(LocalDate.now().plusDays(1), LocalDate.now().plusDays(4)));

        tentativeReservationRepository.save(reservation);

        PlantReservation reservation1 = new PlantReservation();

        reservation1.setSchedule(BusinessPeriod.of(LocalDate.now().plusDays(10), LocalDate.now().plusDays(19)));

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                1L,
                LocalDate.now().plusDays(1),
                LocalDate.now().plusDays(4));

        if(availableItems.size() != 0) {
            for(PlantInventoryItem item: availableItems){
                if (item.getId() == 1L){
                    reservation1.setPlant(item);
                }
            }
        }
        plantReservationRepository.save(reservation1);


        MvcResult result = mockMvc.perform(post("/items/1/requestTentativeReservation")
                .content(mapper.writeValueAsString(reservation))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", not(isEmptyOrNullString())))
                .andReturn();



        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
//

    }

}
