package com.example.demo.sales.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TentativeReservationRepository extends JpaRepository<TentativeReservation, Long> {

    public TentativeReservation findByItemId(Long id);
}
