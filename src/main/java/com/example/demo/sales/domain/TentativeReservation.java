package com.example.demo.sales.domain;


import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Data
public class TentativeReservation {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private BusinessPeriod requestDate;

    @Enumerated(EnumType.STRING)
    TRStatus TRStatus;


    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(updatable = false)
    private LocalDate created_At;

    @PrePersist
    private void onCreate() {
        this.created_At = LocalDate.now();
    }

    @ManyToOne
    PlantInventoryItem item;
}
