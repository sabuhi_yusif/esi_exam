package com.example.demo.inventory.rest;

import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.TentativeReservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("")
public class InventoryRestController {

    @Autowired
    InventoryService inventoryService;


    @PostMapping("/items/{itemId}/requestTentativeReservation")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<TentativeReservation> submitTentativeReservation(@PathVariable("itemId") Long itemId, @RequestBody TentativeReservation tentativeReservation) {

        return inventoryService.submitTentativeReservation(itemId, tentativeReservation);


    }


}
