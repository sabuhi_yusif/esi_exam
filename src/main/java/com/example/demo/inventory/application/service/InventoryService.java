package com.example.demo.inventory.application.service;

import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    TentativeReservationRepository tentativeReservationRepository;

    public List<PlantInventoryEntryDTO> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryEntry> entries = inventoryRepository.findAvailablePlants(name, startDate, endDate);
        return plantInventoryEntryAssembler.toResources(entries);
    }

    private PlantReservation getPlantReservationByPlantId(Long id){
        PlantReservation reservation = plantReservationRepository.findPlantReservationByPlantId(id);

        return reservation;
    }

    public ResponseEntity<TentativeReservation> submitTentativeReservation(Long itemId, TentativeReservation tentativeReservation){
        PlantReservation reservation = getPlantReservationByPlantId(itemId);
        if (reservation == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        //If reservation exosts for the item then reject
        if (tentativeReservationRepository.findByItemId(itemId) != null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

//        if (tentativeReservationRepository.findByItemId(itemId).getCreated_At().plusDays(2).isAfter(LocalDate.now())){
//            return new ResponseEntity<>(HttpStatus.CONFLICT);
//        }
        LocalDate reservationStartDate = reservation.getSchedule().getStartDate();
        LocalDate tentativeReservationStartDate = tentativeReservation.getRequestDate().getStartDate();

        if (LocalDate.now().isAfter(reservationStartDate.minusDays(5))){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }


        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                itemId,
                tentativeReservation.getRequestDate().getStartDate(),
                tentativeReservation.getRequestDate().getEndDate());

        if(availableItems.size() == 0) {
            tentativeReservation.setTRStatus(TRStatus.REJECTED);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }


        tentativeReservation.setTRStatus(TRStatus.ACCEPTED);
        tentativeReservation.setItem(availableItems.get(0));

        return new ResponseEntity<>(tentativeReservationRepository.save(tentativeReservation), HttpStatus.OK);

    }

}
